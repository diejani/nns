# Test different NN architectures against each other

## Available NNs:
 - [x] Multi Layer Perceptron
 - [x] Recurrent Neural Network (single hidden Layer)
   - [ ] multi layer RNN
 - [ ] Convolutional Neural Network

## ToDos
1. set equal initial params for all NNs to be tested
2. pick problems to test NNs on (Kaggle)
