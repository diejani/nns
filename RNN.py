#/usr/bin/env python

import torch
import torch.nn as nn
import MLP

class RNN(MLP):
    def __init__(self, D_in=200, H=[128], D_out=2):
        super(RNN,self).__init__()

    def forward(self, input, prev_state):
        x = torch.cat((input, prev_state), 1) # concatenate the remebered previous hidden state with the input features
        for level, layer in enumerate(self.layers):
            x = layer(x)
            if level = 1: # ToDo multi-level RNN
                current_state = torch.tensor(x)
        return x, current_state
