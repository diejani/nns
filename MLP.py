#/usr/bin/env python
#coding: UTF-8

import torch
from torch import nn

class MLP(torch.nn.Module):
    def __init__(self, D_in=200, H=[128, 30], D_out=2, dropout_f = 0.5):
        layers = []
        
        layers.append(nn.Linear(D_in, H[0]))
        layers.append(nn.ReLU())

        for i in range(len(H) -1):
            layers.append(nn.Linear(H[i], H[i+1]))
            layers.append(nn.ReLU())
            layers.append(torch.nn.Dropout(dropout_f))

        layers.append(nn.Linear(H[-1], D_out))
        layers.append(nn.Softmax(dim=1))

        super(MLP,  self).__init()
        self.layers = nn.ModuleList(layers)

    def forward(self, x):
        for layer in self.layers:
            x = layer(x)
        return x
